#ifndef RC_Recorder_h
#define RC_Recorder_h

#include "WProgram.h"
#include "SdFat.h"
#include "RC_Config.h"
#include "RC_DataStructures.h"
#include "RC_SDManager.h"
#include "RC_Error.h"
#include "Audio.h"

extern SdFat SD;
extern RC_Error error;
extern RC_SDManager SDReader;
extern RC_Config config;
extern int mode;

#define SAMPLE_RATE 44100
#define BITS_PER_SAMPLE 16

typedef char filename[13];

class RC_Recorder
{
    public:
        // Initialize
        bool init();

        unsigned long LastFileNum();
        bool isStereo();
        bool gainUp();
        bool gainDown();
        void updateSettings();

        bool startRecording();
        bool keepRecording();
        bool stopRecording();

        bool startPlayer();
        bool selectPrev();
        bool selectNext();
        bool isPlaying();
        unsigned long playerPos();
        unsigned long playerLen();
        bool startPlayingCurrent();
        void stopPlaying();
        bool keepPlaying();
        void removeCurrent();

        unsigned long timerec;
        AudioAnalyzePeak* getPeak(byte num);
        filename fileList[FILES_IN_PLAYER];
        byte listPointer;
        filename currentFileName ;

    protected:
        File frec;
        int myInput;
        unsigned long byterec;
        long int lastNum;
        bool enterDataFolder();
        unsigned long getLastFile();
        unsigned long getLastFileNum();
        String getFilename(unsigned long num);
        byte channelsNum();
        void initWavHeader();
        bool writeWavHeader();
        bool updateWavHeader();
        unsigned long getPlayerFiles();
        bool prevPlayerState = false;
        T_WavHeader header;
        uint32_t writeCount = 0;
        uint32_t writeTime = 0;
        uint32_t currentFileNum = 0;
        uint32_t nextFileNum,prevFileNum;
        int lastmode;

};

#endif


