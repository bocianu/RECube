#ifndef RC_SDManager_h
#define RC_SDManager_h

#include "WProgram.h"
#include "SdFat.h"
#include "RC_Config.h"
#include "RC_HardwareSetup.h"
#include "RC_DataStructures.h"
#include "RC_Error.h"

extern SdFat SD;
extern T_SDInfo SDInfo;
extern RC_Error error;

class RC_SDManager
{

    public:
        bool mountCard(bool countFree=false);
        bool cardPresent();
    protected:
        uint32_t cardSize;
};

#endif


