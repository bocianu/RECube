#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "OLED1306i2c.h"
#include "oledfont.c"

void OLED1306i2c::init(int sda, int scl)
{
  _sda = sda;
  _scl = scl;
  pinMode(sda, OUTPUT);
  pinMode(scl, OUTPUT);
}


void OLED1306i2c::IIC_Start()
{
  digitalWrite(_scl, HIGH);
  digitalWrite(_sda, HIGH);
  digitalWrite(_sda, LOW);
  digitalWrite(_scl, LOW);
}


void OLED1306i2c::IIC_Stop()
{
  digitalWrite(_scl, LOW);
  digitalWrite(_sda, LOW);
  digitalWrite(_scl, HIGH);
  digitalWrite(_sda, HIGH);
}


void OLED1306i2c::Write_IIC_Byte(unsigned char IIC_Byte)
{
  unsigned char i;
  for(i=0;i<8;i++)
  {
    if((IIC_Byte << i) & 0x80)digitalWrite(_sda, HIGH);
    else
      digitalWrite(_sda, LOW);
    digitalWrite(_scl, HIGH);
    digitalWrite(_scl, LOW);
//    IIC_Byte<<=1;
   }
  digitalWrite(_sda, HIGH);
  digitalWrite(_scl, HIGH);
  digitalWrite(_scl, LOW);
}


void OLED1306i2c::Write_IIC_Command(unsigned char IIC_Command)
{
   IIC_Start();
   Write_IIC_Byte(0x78);  //Slave address,SA0=0
   Write_IIC_Byte(0x00);	//write command
   Write_IIC_Byte(IIC_Command);
   IIC_Stop();
}


void OLED1306i2c::Begin_IIC_Data()
{
   IIC_Start();
   Write_IIC_Byte(0x78);
   Write_IIC_Byte(0x40);	//write data
}


void OLED1306i2c::IIC_SetPos(unsigned char x, unsigned char y)
{
  IIC_Start();
  Write_IIC_Byte(0x78);  //Slave address,SA0=0
  Write_IIC_Byte(0x00);	//write command
  
  Write_IIC_Byte(0xb0+y);
  Write_IIC_Byte(((x&0xf0)>>4)|0x10);//|0x10
  Write_IIC_Byte((x&0x0f));//|0x01

//  Write_IIC_Byte((x&0x0f)|0x01);//|0x01
  
  IIC_Stop();
}


void OLED1306i2c::FillScreen(unsigned char fill_Data)
{
  unsigned char m,n;
  for(m=0;m<8;m++)
  {
    Write_IIC_Command(0xb0+m);	//page0-page1
    Write_IIC_Command(0x00);		//low column start address
    Write_IIC_Command(0x10);		//high column start address
    Begin_IIC_Data();
    for(n=0;n<128;n++)
    {
      Write_IIC_Byte(fill_Data);
    }
    IIC_Stop();
  }
}

void OLED1306i2c::FillLine(unsigned char row, unsigned char fill_Data, unsigned char len)
{
   unsigned char n;
   Write_IIC_Command(0xb0+row);	//page0-page1
   Write_IIC_Command(0x00);		//low column start address
   Write_IIC_Command(0x10);		//high column start address
   Begin_IIC_Data();
   for(n=0;n<len;n++)
   {
     Write_IIC_Byte(fill_Data);
   }
   IIC_Stop();
}



void OLED1306i2c::Char_8(unsigned char x, unsigned char y, unsigned char w, const char ch[], const char font[], bool invert)
{
  unsigned char c,i,j=0;
  while(ch[j] != '\0')
  {
    c = ch[j] - 32;
    if(x>121)
    {
      x=0;
      y++;
    }
    IIC_SetPos(x,y);
    Begin_IIC_Data();
    for(i=0;i<w;i++)
    {
      Write_IIC_Byte((invert)?~font[c*w+i]:font[c*w+i]);
    }
    IIC_Stop();
    x += w;
    j++;
  }
}

void OLED1306i2c::CharSmall(unsigned char x, unsigned char y, const char ch[], bool invert)
{
	Char_8(x,y,7,ch,fontSmall,invert);
}


void OLED1306i2c::Char(unsigned char x, unsigned char y, const char ch[], bool invert)
{
	Char_8(x,y,6,ch,minecraft_6x8,invert);
}

void OLED1306i2c::CharC(unsigned char y, const char ch[], bool invert)
{
	unsigned char offset = (127 - (strlen(ch)*6)) / 2;
	Char(offset, y, ch);
}


void OLED1306i2c::Char_16(unsigned char x, unsigned char y, unsigned char w, const char ch[], const char font[], bool invert)
{
	unsigned char c=0,i=0,j=0;
	char skip=0;
	while (ch[j]!='\0')
	{
		c =ch[j]-32;
		if(x>120)
		{
			x=0;
			y++;
		}
		IIC_SetPos(x,y);
		skip = 0;
		Begin_IIC_Data();
		for(i=0;i<w;i++)
		{
			if (font[(c*w*2)+i]+font[(c*w*2)+i+w]==0) {
				skip++;
			} else {
				Write_IIC_Byte((invert)?~font[c*24+i]:font[c*24+i]);
			}
		}
		IIC_Stop();
		IIC_SetPos(x,y+1);
		Begin_IIC_Data();
		for(i=0;i<w;i++)
		{
			if (font[(c*w*2)+i]+font[(c*w*2)+i+w]!=0) {
				Write_IIC_Byte((invert)?~font[(c*w*2)+i+w]:font[(c*w*2)+i+w]);
			}
		}
		IIC_Stop();
		if (skip > (w-2)) skip=(w/3);
		x+=w-skip+1;
		j++;
	}
}

void OLED1306i2c::CharBig(unsigned char x, unsigned char y, const char ch[], bool invert) {
	Char_16(x,y,12,ch,font12x16,invert);
}

void OLED1306i2c::CharBigC(unsigned char y, const char ch[], bool invert)
{
	unsigned char offset = (127 - (strlen(ch)*10)) / 2;
	CharBig(offset, y, ch, invert);
}


void OLED1306i2c::Draw_BMP(unsigned char x0, unsigned char y0, unsigned char x1, unsigned char y1,const char BMP[])
{
  unsigned int j=0;
  unsigned char x,y;
  
  if(y1%8==0)
    y=y1/8;
  else
    y=y1/8+1;
  for(y=y0;y<y1;y++)
  {
    IIC_SetPos(x0,y);
    Begin_IIC_Data();
    for(x=x0;x<x1;x++)
    {
      Write_IIC_Byte(BMP[j++]);
    }
    IIC_Stop();
  }
}


void OLED1306i2c::Initial()
{
  Write_IIC_Command(0xAE);// *** display off (AF on)

  Write_IIC_Command(0x20);// *** Set memory addressing mode
  Write_IIC_Command(0x02);// 0 horizontal, 1 vertical, 2 page, 3 invalid

  Write_IIC_Command(0x00);// *** Set Memory Addressing Mode (page addresing) lower nibble
  Write_IIC_Command(0x10);// *** Set Memory Addressing Mode (page addresing) higher nibble
  
  Write_IIC_Command(0x40);// *** Set display RAM display start line register (40 to 7F)

  Write_IIC_Command(0x81);// *** set Contrast
  Write_IIC_Command(0x40);// value (was CF)

  Write_IIC_Command(0xA1);// *** Segment remap - LR mirror A0
  
  Write_IIC_Command(0xC8);// *** Set COM Output Scan Direction (C0 norma - c8 remapped)  upside down
  
  Write_IIC_Command(0xA8);// *** set MUX ratio
  Write_IIC_Command(0x3F);// from 16MUX to 64MUX, RESET=111111b - from 0 to 14 are invalid entry. 
  
  Write_IIC_Command(0xD3);// *** Set Display Offset (vertical)
  Write_IIC_Command(0x00);// 0-63
  
  Write_IIC_Command(0xD5);// *** Set Display Clock Divide Ratio/Oscillator Frequency
  Write_IIC_Command(0x80);// 4 bits freq - 4 bits divider
  
  Write_IIC_Command(0xD9);// *** Set Set Pre-charge Period
  Write_IIC_Command(0xF1);// 
  
  Write_IIC_Command(0xDA);// *** Set COM Pins Hardware Configuration 
  Write_IIC_Command(0x12);// 

  Write_IIC_Command(0xDB);// *** Set VCOMH Deselect Level
  Write_IIC_Command(0x20);// was 40 (brighter)
  
  
  
  Write_IIC_Command(0xA4);// *** A4 Display RAM content, A5 all display on

  Write_IIC_Command(0xA6);// *** A6 - normal display. A7 - negative

  Write_IIC_Command(0x8D);// *** Charge Pump Setting
  Write_IIC_Command(0x14);// 14 - on, 10 - off
  
  Write_IIC_Command(0xAF);// *** display on (AE off)
}

