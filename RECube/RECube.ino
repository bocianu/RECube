#include "RC_DataStructures.h"
#include "RC_HardwareSetup.h"
#include "RC_Config.h"
#include "RC_Error.h"
#include "RC_Recorder.h"
#include "RC_SDManager.h"
#include "MyBounce.h"
#include <Wire.h>
#include <SPI.h>
#include <Audio.h>
#include <SdFat.h>
#include <EEPROM.h>
#include "RC_Gui.h"

Bounce buttonRecord = Bounce(PIN_RECORD, BOUNCE_TIME);
Bounce buttonBack 	= Bounce(PIN_BACK, 	 BOUNCE_TIME);
Bounce buttonOK 	= Bounce(PIN_OK, 	 BOUNCE_TIME);
Bounce buttonUp 	= Bounce(PIN_UP, 	 BOUNCE_TIME);
Bounce buttonDown 	= Bounce(PIN_DOWN, 	 BOUNCE_TIME);

// Config object
RC_Config config;

// SD objects
SdFat SD;
T_SDInfo SDInfo;
RC_SDManager SDReader;

// Error Message object
RC_Error error(&mode,MODE_ERROR);

// Recorder object
RC_Recorder recorder;

// Gui
RC_Gui gui;

// Set starting mode
int mode = MODE_STANDBY;
elapsedMicros usec = 0;

// *********************************************************************
// ********************************************************* SETUP
// *********************************************************************

void setup()   {

#ifdef SERIAL_DEBUG
    while (!Serial) {} // wait for Leonardo
#endif
    delay(500);

    // Configure the pushbutton pins
    pinMode(PIN_UP, 	INPUT_PULLUP);
    pinMode(PIN_DOWN, 	INPUT_PULLUP);
    pinMode(PIN_BACK, 	INPUT_PULLUP);
    pinMode(PIN_OK, 	INPUT_PULLUP);
    pinMode(PIN_RECORD, INPUT_PULLUP);
    pinMode(PIN_LED, OUTPUT);

    // initialize config
    config.init();
    //config.setInput(AUDIO_INPUT_MIC);
    //config.setMicGain(0);

    // Initialize the SD card
    SPI.setMOSI(7);
    SPI.setSCK(14);

    // Oled
    gui.init();
    gui.showScreen(SCREEN_INIT);

#ifdef SERIAL_DEBUG
    Serial.println("Initializing SD Card");
#endif

    // Mount SD Card
    if (SDReader.mountCard()) {
        if (recorder.init()) {
#ifdef SERIAL_DEBUG
            showSDInfo();
#endif
        }
    };

	// reset init pulses
    buttonRecord.update();
    buttonOK.update();
    buttonUp.update();
    buttonDown.update();
    buttonBack.update();

}

// *********************************************************************
// ********************************************************* LOOP
// *********************************************************************

void loop() {

    // First, read the buttons
    buttonRecord.update();
    buttonOK.update();
    buttonUp.update();
    buttonDown.update();
    buttonBack.update();

#ifdef SERIAL_DEBUG
    // fake serial keypress routines - TO BE REMOVED
    int incomingByte;
    if (Serial.available() > 0) {
        incomingByte = Serial.read();
        if (incomingByte == 'R') {
            buttonRecord.fakePress(0);
        }
        if (incomingByte == 'O') {
            buttonOK.fakePress(0);
        }
        if (incomingByte == 'U') {
            buttonUp.fakePress(0);
        }
        if (incomingByte == 'D') {
            buttonDown.fakePress(0);
        }
        if (incomingByte == 'B') {
            buttonBack.fakePress(0);
        }

    }
#endif

    // ********************************************* UP'n'DOWN REPEAT

    if (buttonUp.read()==LOW && buttonUp.duration()>200) {
        buttonUp.rebounce(50);
    }
    if (buttonDown.read()==LOW && buttonDown.duration()>200) {
        buttonDown.rebounce(50);
    }


    // ************************************************** STANDBY
    if (mode == MODE_STANDBY) {

        if (buttonRecord.fallingEdge()) {
            recorder.startRecording();
        }
        else if (buttonOK.risingEdge()) {
            if (recorder.startPlayer()) {

            }
            else {
                //Serial.println("No files to play");
            }
        }
        else if (buttonBack.risingEdge()) {
            mode = MODE_OPTIONS;
        }
        else if (buttonUp.fallingEdge()) {
            if (recorder.gainUp()) gui.forceRedraw(MID_UPDATE);
        }
        else if (buttonDown.fallingEdge()) {
            if (recorder.gainDown()) gui.forceRedraw(MID_UPDATE);
        }

    }

    // ************************************************** RECORD
    else if (mode == MODE_RECORD) {

        if (buttonRecord.fallingEdge()) {
            recorder.stopRecording();
        }
        else {
            recorder.keepRecording();
        }

    }

    // ************************************************** PLAY
    else if (mode == MODE_PLAY) {

        if (buttonRecord.fallingEdge()) {
            if (recorder.isPlaying()) {
                recorder.stopPlaying();
            }
            recorder.startRecording();
        }
        else if (buttonOK.risingEdge() && buttonOK.lastduration()>2000) {
            if (!recorder.isPlaying()) {
                recorder.removeCurrent();
                gui.forceRedraw(MID_UPDATE);
            }
        }
        else if (buttonOK.risingEdge()) {
            if (!recorder.isPlaying()) {
                recorder.startPlayingCurrent();
            } else {
                recorder.stopPlaying();
            }
            gui.forceRedraw(MID_UPDATE);
        }
        else if (buttonBack.risingEdge()) {
            if (!recorder.isPlaying()) {
                mode = MODE_STANDBY;
            } else {
                recorder.stopPlaying();
            }
            gui.forceRedraw(MID_UPDATE);
        }
        else if (buttonUp.fallingEdge()) {
            if (!recorder.isPlaying()) {
                recorder.selectPrev();
                gui.forceRedraw(MID_UPDATE);
            }
        }
        else if (buttonDown.fallingEdge()) {
            if (!recorder.isPlaying()) {
                recorder.selectNext();
                gui.forceRedraw(MID_UPDATE);
            }
        }
        else {
            if (!recorder.keepPlaying()) gui.forceRedraw(MID_UPDATE);
        }

    }

    // ************************************************** OPTIONS
    else if (mode == MODE_OPTIONS) {

        if (buttonRecord.fallingEdge()) {
            recorder.startRecording();
        }
        else if (buttonOK.risingEdge()) {
            gui.optionBack();
        }
        else if (buttonBack.risingEdge()) {
            gui.optionEdit();
        }
        else if (buttonUp.fallingEdge()) {
            gui.optionUp();
        }
        else if (buttonDown.fallingEdge()) {
            gui.optionDown();
        }

    }

    // ************************************************** ERROR
    else if (mode == MODE_ERROR) {
        if (buttonRecord.fallingEdge() || buttonOK.fallingEdge()) {
            gui.showScreen(SCREEN_REBOOT);
			delay(500);
            CPU_RESTART
        }
        if (usec>100000) {
            usec = 0;
            if (digitalRead(PIN_LED)==LOW) {
                digitalWrite(PIN_LED,HIGH);
            } else {
                digitalWrite(PIN_LED,LOW);
            }
        }

    }

    gui.update(mode);
}

#ifdef SERIAL_DEBUG

void showSDInfo() {
    Serial.print("SD Status: ");
    Serial.println((SDInfo.status == 0)?"READY":"ERROR");
    Serial.print("Card type: ");
    Serial.println(SDInfo.type);
    Serial.print("Filesystem: ");
    Serial.println(SDInfo.filesystem);
    Serial.print("Card size: ");
    Serial.print(SDInfo.size,2);
    Serial.println("GB");
    Serial.print("Free space: ");
    Serial.print(SDInfo.free,2);
    Serial.println("GB");
    if (SDInfo.status == 0) {
        Serial.print("Last file number: ");
        Serial.println(recorder.LastFileNum());
    }
}

#endif




