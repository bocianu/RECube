#ifndef RC_Gui_h
#define RC_Gui_h

#include "WProgram.h"
#include <Audio.h>
#include "RC_HardwareSetup.h"
#include "RC_Config.h"
#include "RC_DataStructures.h"
#include "RC_Recorder.h"
#include "RC_Error.h"
#include "OLED1306i2c.h"
#include "oledfont.c"

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

#define SCREEN_INIT 	0
#define SCREEN_REBOOT 	1
#define SCREEN_SDINFO 	2
#define SCREEN_ABOUT 	3

#define NO_UPDATE	0
#define MID_UPDATE 	1
#define ALL_UPDATE 	2

extern RC_Config config;
extern RC_Recorder recorder;
extern RC_Error error;
extern T_SDInfo SDInfo;
extern elapsedMicros usec;

class RC_Gui {
    public:
        void init();
        void showScreen(int type);
        void update(int mode);
        void forceRedraw(char lvl=2);
        void optionUp();
        void optionDown();
        void optionEdit();
        void optionBack();

    protected:
        char forceUpdate = NO_UPDATE;
        void redraw(int mode);
        OLED1306i2c display;
        int prev_mode = -1;
        unsigned long prevTime;
        void secToTime(unsigned long sec, char* buffer);
        char line[21];
        String sline;
        AudioAnalyzePeak* peakL;
        AudioAnalyzePeak* peakR;
        byte peakLMax = 0;
        byte peakRMax = 0;
        elapsedMicros peakLTime = 0;
        elapsedMicros peakRTime = 0;
        void showPeakMeters(byte lRow, byte rRow);
        byte optPointer = 1;
        bool optInEdit = false;
};



#endif

