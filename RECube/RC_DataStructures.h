#ifndef RC_DataStructures_h
#define RC_DataStructures_h

#include "WProgram.h"
#include "Audio.h"

// SD Card Info
typedef struct {
    bool status = 99;
    char type[6] = "None";
    char filesystem[6] = "None";
    float size = 0;
    float free = 0;
} T_SDInfo;

// WAV header
typedef struct {
    char riff[4];
    uint32_t size;
    char wave[4];
    char fmt[4];
    uint32_t chunksize;
    uint16_t type;
    uint16_t channels;
    uint32_t samplerate;
    uint32_t bytespersec;
    uint16_t blockalign;
    uint16_t bps;
    char data[4];
    uint32_t datasize;
} T_WavHeader;

#endif


