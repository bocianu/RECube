
#ifndef _IIC_without_ACK_H
#define _IIC_without_ACK_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

//#include <Adafruit_GFX.h>

#define BLACK 0
#define WHITE 1

class OLED1306i2c
{
 public:
  void init(int sda, int scl);
  void IIC_Start();
  void IIC_Stop();
  void Write_IIC_Byte(unsigned char IIC_Byte);
  void Write_IIC_Command(unsigned char IIC_Command);
  void Begin_IIC_Data();
  void IIC_SetPos(unsigned char x, unsigned char y);
  void FillScreen(unsigned char fill_Data);
  void FillLine(unsigned char row, unsigned char fill_Data, unsigned char len=128);
  void Char_8(unsigned char x, unsigned char y, unsigned char w, const char ch[], const char font[], bool invert=false);
  void Char(unsigned char x, unsigned char y, const char ch[], bool invert=false);
  void CharC(unsigned char y, const char ch[], bool invert=false);
  void CharSmall(unsigned char x, unsigned char y, const char ch[], bool invert=false);
  void Char_16(unsigned char x, unsigned char y, unsigned char w, const char ch[], const char font[], bool invert=false);
  void CharBig(unsigned char x, unsigned char y, const char ch[], bool invert=false);
  void CharBigC(unsigned char y, const char ch[], bool invert=false);
  void Draw_BMP(unsigned char x0, unsigned char y0, unsigned char x1, unsigned char y1,const char BMP[]);
  void Initial();
  
  private:
  int _sda, _scl;
};
#endif

