#ifndef RC_Error_h
#define RC_Error_h

#include "WProgram.h"

class RC_Error
{

public:
	// Initialize
  RC_Error(int* mode,int error_mode); 
	
  void set(char* txt);
  void set(String txt);
  void set(const char* txt);
  void add(char* txt);
  bool active();
  char* get();
  char* display();
  
protected:
  int* mode;
  char msg[64];
  int error_mode;
  uint8_t state;
  void mute();
  void raise();
};

#endif


