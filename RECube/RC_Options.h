typedef void ( * setResult )(char *);
typedef bool (* change)();

extern RC_Gui gui;
extern int mode;

// ************************************************ INIT

void empty(char * result) {
    stpcpy(result,"");
}

bool dummy() {
    return false;
}

bool refresh() {
	gui.forceRedraw(MID_UPDATE);
    return false;
}


class RC_Option {
    public:
        char name[13]="option";
        setResult result = empty;
        change changeUp = dummy;
        change changeDown = dummy;
        change onEdit = refresh;
        change onLeave = refresh;
};

#define OPTIONS_NUMBER 6
#define OPTIONS_ONSCREEN 3

RC_Option options[OPTIONS_NUMBER];

// ************************************************ INPUT

void InputResult(char * result) {
    strcpy(result,config.getInput()!=AUDIO_INPUT_MIC?"LINE":"MIC");
}

bool InputChange() {
    if (config.getInput()==AUDIO_INPUT_MIC) {
        config.setInput(AUDIO_INPUT_LINEIN);
    } else {
        config.setInput(AUDIO_INPUT_MIC);
    };
    recorder.updateSettings();
    return true;
}

// ************************************************ HEADPHONES

void HpResult(char * result) {
    String sval = (int)(config.getHeadphonesVol()*10.0);
    sval.toCharArray(result,4);
}

bool HpUp() {
    if (config.setHeadphonesVol(config.getHeadphonesVol()+0.1)) {
        recorder.updateSettings();
        return true;
    }
    return false;
}

bool HpDown() {
    if (config.setHeadphonesVol(config.getHeadphonesVol()-0.1)) {
        recorder.updateSettings();
        return true;
    }
    return false;
}

// ************************************************ LINE OUT

void LoResult(char * result) {
    String sval = config.getLineOutLevel()-13;
    sval.toCharArray(result,4);
}


bool LoUp() {
    if (config.setLineOutLevel(config.getLineOutLevel()+1)) {
        recorder.updateSettings();
        return true;
    }
    return false;
}

bool LoDown() {
    if (config.setLineOutLevel(config.getLineOutLevel()-1)) {
        recorder.updateSettings();
        return true;
    }
    return false;
}

// ************************************************ SDInfo

bool showSd() {
    gui.showScreen(SCREEN_SDINFO);
    return true;
}

bool showAbout() {
    gui.showScreen(SCREEN_ABOUT);
    return true;
}

bool redraw() {
    gui.forceRedraw();
    return true;
}

bool reboot() {
	gui.showScreen(SCREEN_REBOOT);
	delay(1000);
	CPU_RESTART
	return false; // FALSE NEVER RETURNS, muaha ha ha ha!
}

// ************************************************ INIT

void optInit() {
    strcpy(options[0].name,"Restart");
    options[0].onEdit = reboot;
    options[0].onLeave = redraw;

    strcpy(options[1].name,"Input: ");
    options[1].result = InputResult;
    options[1].changeUp = InputChange;
    options[1].changeDown = InputChange;

    strcpy(options[2].name,"HPhones Vol: ");
    options[2].result = HpResult;
    options[2].changeUp = HpUp;
    options[2].changeDown = HpDown;

    strcpy(options[3].name,"LineOut Vol: ");
    options[3].result = LoResult;
    options[3].changeUp = LoUp;
    options[3].changeDown = LoDown;

    strcpy(options[4].name,"Show SD Info");
    options[4].onEdit = showSd;
    options[4].onLeave = redraw;

    strcpy(options[5].name,"About ReCube");
    options[5].onEdit = showAbout;
    options[5].onLeave = redraw;
};
