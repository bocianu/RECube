#include "RC_SDManager.h"

bool RC_SDManager::cardPresent() {
    return SD.cardBegin(SD_CS, SPI_HALF_SPEED);
}

bool RC_SDManager::mountCard(bool countFree) {
    SDInfo.status = 0;
    if (!cardPresent()) {
        SDInfo.status = 1;
        error.set("SD Card Initialization Failed");
        return false;
    }

    cardSize = SD.card()->cardSize();
    if (cardSize == 0) {
        SDInfo.status = 2;
        error.set("cardSize failed");
        return false;
    }

    // set type of card
    switch(SD.card()->type()) {
    case SD_CARD_TYPE_SD1:
        strcpy(SDInfo.type, "SD1");
        break;
    case SD_CARD_TYPE_SD2:
        strcpy(SDInfo.type, "SD2");
        break;
    case SD_CARD_TYPE_SDHC:
        strcpy(SDInfo.type, "SDHC");
        break;
    default:
        strcpy(SDInfo.type, "Other");
    }

    // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
    if (!SD.fsBegin()) {
        SDInfo.status = 3;
        error.set("File System initialization failed");
        return false;
    }

    // print the type and size of the first FAT-type volume
    String str;
    str = String(SD.vol()->fatType());
    str = "FAT" + str;
    str.toCharArray(SDInfo.filesystem,6);

    uint32_t blockPerCluster = SD.vol()->blocksPerCluster();
    SDInfo.size = (float)(blockPerCluster * SD.vol()->clusterCount()) / (2 * 1024 * 1024);
	//SDInfo.free = (float)(blockPerCluster * SD.vol()->freeClusterCount()) / (2 * 1024 * 1024);	
    return true;
}


