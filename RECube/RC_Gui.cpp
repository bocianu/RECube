#include "RC_Gui.h"

#define PEAK_LEN 16
// usec
#define PEAK_DELAY 100000
// sec
#define PEAK_REMAIN 3*1000000

#define PEAK_BLOCK '#'
#define PEAK_HOLD '$'
#define PEAK_END_LEFT '('
#define PEAK_END_RIGHT ')'

#include "RC_Options.h"

void RC_Gui::init() {
	display.init(OLED_SDA, OLED_SCL);
	display.Initial();
	display.FillScreen(0x00);
	peakL = recorder.getPeak(1);
	peakR = recorder.getPeak(2);

	// options init;
	optInit();

}

void RC_Gui::forceRedraw(char lvl) {
	forceUpdate = lvl;
}

// *********************************************************************
// ************************************************* INIT DRAW
// *********************************************************************

void RC_Gui::redraw(int mode) {
	switch (mode) {

	case MODE_STANDBY :    // ************************** STANDBY

		display.FillScreen(0x00);

		display.FillLine(0,0xff);
		display.FillLine(1,0xff);
		display.CharBig(4,0,"RECORDER", true);

		sline = "SD Status: " ;
		sline += ((SDInfo.status == 0)?"READY":"ERROR");
		sline.toCharArray(line,21);
		display.CharC(3,line);

		sline = "Source: " ;
		sline += (config.getInput()!=AUDIO_INPUT_MIC)?"LINE":"MIC";
		sline.toCharArray(line,21);
		display.CharC(4,line);

		display.FillLine(7,0xff);
		display.CharSmall(0,7,"<OPTIONS   PLAYER>",true);

		break;

	case MODE_RECORD :		// ************************** RECORD

		display.FillScreen(0x00);
		display.CharBigC(0,"RECORDING");
		sline = "File number: ";
		sline += recorder.LastFileNum() + 1;
		sline.toCharArray(line,21);
		display.CharC(3,line);
		prevTime = 0;

		break;

	case MODE_PLAY :		// ************************** PLAYER

		display.FillScreen(0x00);
		display.FillLine(0,0xff);
		display.FillLine(1,0xff);
		display.CharBig(4,0,"PLAYER", true);

		break;

	case MODE_OPTIONS :		// ************************** OPTIONS

		display.FillScreen(0x00);
		display.FillLine(0,0xff);
		display.FillLine(1,0xff);
		display.CharBig(4,0,"OPTIONS", true);


		break;

	case MODE_ERROR :		// ************************** ERROR

		display.FillScreen(0x00);
		display.CharBig(0,0,"ERROR :");
		display.Char(0,2,error.display());

		break;
	}
}

// *********************************************************************
// ************************************************* CYCLE UPDATE
// *********************************************************************

void RC_Gui::update(int mode) {
	byte igain;
	byte optOffset = 0;
	if ( prev_mode != mode ) {
		forceUpdate = MID_UPDATE;
		redraw(mode);
		prev_mode = mode;
	} else if (forceUpdate==ALL_UPDATE) {
		forceUpdate = MID_UPDATE;
		redraw(mode);
	};

	switch (mode) {

	case MODE_STANDBY :		// *************************** STANDBY
		if (forceUpdate==MID_UPDATE) {
			display.FillLine(5,0);
			sline = "Input Gain: " ;
			igain = (config.getInput()==AUDIO_INPUT_MIC)?config.getMicGain():config.getLineInLevel();
			sline += String(igain);
			sline.toCharArray(line,21);
			display.CharC(5,line);
		}
		showPeakMeters(2,6);
		break;

	case MODE_RECORD :		// *************************** RECORD
		if (prevTime != recorder.timerec) {
			prevTime = recorder.timerec;
			secToTime(recorder.timerec,line);
			display.FillLine(4,0);
			display.FillLine(5,0);
			display.CharBig(36,4,line);
		};
		showPeakMeters(2,6);
		break;

	case MODE_PLAY :		// *************************** PLAYER
		if (forceUpdate==MID_UPDATE) {
			display.FillLine(3,0);
			display.FillLine(4,0);
			display.FillLine(5,0);
			if (recorder.isPlaying()) {  
				display.CharBig(7,3,recorder.currentFileName);
				prevTime = recorder.playerLen()-recorder.playerPos();
				secToTime(prevTime,line);
				display.CharC(5,line);
				display.FillLine(7,0xff);
				display.CharSmall(0,7,"<BACK        STOP>",true);

			} else {
				for (byte i=0; i<FILES_IN_PLAYER; i++) {
					if (i==recorder.listPointer) display.CharSmall(6,3+i,">");
					display.Char(16,3+i,recorder.fileList[i]);
				}
				display.FillLine(7,0xff);
				display.CharSmall(0,7,"<BACK        PLAY>",true);
			}
		}
		if (prevTime!=recorder.playerLen()-recorder.playerPos()) {
			prevTime=recorder.playerLen()-recorder.playerPos();
			secToTime(prevTime,line);
			display.CharC(5,line);
		}
		break;

	case MODE_OPTIONS :		// *************************** OPTIONS
		
		if (forceUpdate==MID_UPDATE) {
			
			display.FillLine(3,0);
			display.FillLine(4,0);
			display.FillLine(5,0);
			
			if (optPointer>=OPTIONS_ONSCREEN-1) {
				optOffset = optPointer-(OPTIONS_ONSCREEN-1);
			}

			for (byte i=0; i<OPTIONS_ONSCREEN; i++) {
				byte optIdx = i+optOffset;
				if (optPointer==optIdx) {
					if (optInEdit) display.FillLine(3+i,0xFF);
					display.CharSmall(2,3+i,">",(optPointer==optIdx && optInEdit));			
				}
				display.Char(12,3+i,options[optIdx].name,(optPointer==optIdx && optInEdit));
				options[optIdx].result(line);
				display.Char(92,3+i,line,(optPointer==optIdx && optInEdit));
			}
			if (!optInEdit) {
				display.FillLine(7,0xff);
				display.CharSmall(0,7,"<EDIT        BACK>",true);
			} else {
				display.FillLine(7,0xff);
				display.CharSmall(0,7,"<OK          BACK>",true);
			}
		}
		break;

	case MODE_ERROR :		// *************************** ERROR
		
		break;
	}
	
	forceUpdate = NO_UPDATE;
}

// *********************************************************************
// ************************************************* STATIC SCREENS
// *********************************************************************

void RC_Gui::showScreen(int type) {
	char txt[4];
	uint32_t blockPerCluster;
	switch (type) {

	case SCREEN_REBOOT :

		display.FillScreen(0x00);
		display.CharBigC(2,"REBOOT");
		break;

	case SCREEN_INIT :

		display.FillScreen(0x00);
		display.CharBig(0,0,"RECube");
		config.getVersion(txt);
		display.Char(62,1,txt);
		display.FillLine(2,2);

		display.CharC(3,"Initializing SD Card");
		display.CharC(5,"Please Wait");

		display.FillLine(6,0x20);
		
		char line[PEAK_LEN+3];
		for (char i=0; i<PEAK_LEN+3; i++)
		{
			memset(line,0,PEAK_LEN);
			memset(line,PEAK_HOLD,i);
			display.CharSmall(0,7,line);
			delay(50);
		}
		break;

	case SCREEN_ABOUT :

		display.FillScreen(0x00);
		display.CharBig(0,0,"RECube");
		config.getVersion(txt);
		display.Char(62,1,txt);
		display.FillLine(2,2);

		display.CharC(3,"Designed & Developed");
		display.CharC(4,"by:");
		display.CharC(5,"bocianu@gmail.com");
	
		display.FillLine(6,0x20);
		
		break;

		
	case SCREEN_SDINFO :		
		
		display.FillScreen(0x00);

		display.CharBig(0,0,"SD CARD INFO");
		display.FillLine(2,2,116);
		
		sline = "SD Status:  " ;
		sline += ((SDInfo.status == 0)?"READY":"ERROR");
		sline.toCharArray(line,21);
		display.Char(0,3,line);

		sline = "Card type:  ";
		sline += SDInfo.type;
		sline.toCharArray(line,21);
		display.Char(0,4,line);

		sline = "Filesystem: ";
		sline += SDInfo.filesystem;
		sline.toCharArray(line,21);
		display.Char(0,5,line);

		sline = "Card size: ";
		sline.toCharArray(line,21);
		dtostrf(SDInfo.size, 6, 2, line+10);
		display.Char(0,6,line);		

		display.Char(0,7,"counting free.");		

		blockPerCluster = SD.vol()->blocksPerCluster();
		SDInfo.size = (float)(blockPerCluster * SD.vol()->clusterCount()) / (2 * 1024 * 1024);
		SDInfo.free = (float)(blockPerCluster * SD.vol()->freeClusterCount()) / (2 * 1024 * 1024);	
		
		sline = "Available: ";
		sline.toCharArray(line,21);
		dtostrf(SDInfo.free, 6, 2, line+10);
		display.Char(0,7,line);		

		break;

	}
}

// *********************************************************************
// ************************************************* HELPERS
// *********************************************************************

void RC_Gui::showPeakMeters(byte lRow,byte rRow) {
	byte peak;
	if (usec>PEAK_DELAY) {
		usec = 0;
		if (peakL->available()) {
			peak = (int)(peakL->read()*PEAK_LEN);
			memset(line,' ',sizeof(line));
			memset(line+1,PEAK_BLOCK,peak);
			if (peakLMax<peak) {
				peakLMax = peak;
				peakLTime = 0;
			}
			if (peakLTime>PEAK_REMAIN) {
				peakLMax = peak;
				peakLTime = 0;
			}
			line[peakLMax] = PEAK_HOLD;
			line[0] = PEAK_END_LEFT;
			line[PEAK_LEN+1] = PEAK_END_RIGHT;
			line[PEAK_LEN+2] = 0;
			display.CharSmall(1,lRow,line);

		};
		if (peakR->available()) {
			peak = (int)(peakR->read()*PEAK_LEN);
			//peak = PEAK_LEN;
			memset(line,' ',sizeof(line));
			memset(line+1,PEAK_BLOCK,peak);
			if (peakRMax<peak) {
				peakRMax = peak;
				peakRTime = 0;
			}
			if (peakRTime>PEAK_REMAIN) {
				peakRMax = peak;
				peakRTime = 0;
			}
			line[peakRMax] = PEAK_HOLD;
			line[0] = PEAK_END_LEFT;
			line[PEAK_LEN+1] = PEAK_END_RIGHT;
			line[PEAK_LEN+2] = 0;
			display.CharSmall(1,rRow,line);

		};
	};
}

void RC_Gui::secToTime(unsigned long sec, char* buffer) {
	byte h,m,s;
	String strl;
	h = sec/3600;
	m = (sec-(h*3600))/60;
	s = sec-(h*3600)-(m*60);
	strl = String(h) ;
	strl += (m<10)?":0":":";
	strl += String(m);
	strl += (s<10)?":0":":";
	strl += String(s);
	strl.toCharArray(buffer,strl.length()+1);
}

void RC_Gui::optionEdit() {
	if (optInEdit) {
		options[optPointer].onLeave();
		forceRedraw();
		optInEdit = false;
	} else {
		options[optPointer].onEdit();
		optInEdit = true;
	}
}

void RC_Gui::optionBack() {
	if (optInEdit) {
		options[optPointer].onLeave();
		forceRedraw();
		optInEdit = false;
	} else {
		mode = MODE_STANDBY;
	}
	
}


void RC_Gui::optionUp() {
	if (!optInEdit) {
		if (optPointer>0) {
			optPointer--;
			forceRedraw(MID_UPDATE);
		}
	} else {
		if (options[optPointer].changeUp()) forceRedraw(MID_UPDATE);
	}
}

void RC_Gui::optionDown() {
	if (!optInEdit) {
		if (optPointer<OPTIONS_NUMBER-1) {
			optPointer++;
			forceRedraw(MID_UPDATE);
		}
	} else {
		if (options[optPointer].changeDown()) forceRedraw(MID_UPDATE);
	}
}
