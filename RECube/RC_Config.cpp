#include "RC_Config.h"
#include <EEPROM.h>
#include "RC_DataStructures.h"

#ifdef SERIAL_DEBUG	
void showConfig(T_Config conf) {
    Serial.println("------------------------------");
    Serial.print("version: ");
    Serial.print(conf.versionH);
    Serial.print(".");
    Serial.println(conf.versionL);
    Serial.print("input: ");
    Serial.println(conf.input);
    Serial.print("micGain: ");
    Serial.println(conf.micGain);
    Serial.print("LineInLevel: ");
    Serial.println(conf.lineInLevel);
    Serial.print("LineOutLevel: ");
    Serial.println(conf.lineOutLevel);
    Serial.println("------------------------------");
}
#endif


void RC_Config::init() {
    T_Config newConfig;
    newConfig = read();

    if (isValid(newConfig)) {
        this->config = newConfig;
    } else {
        store();
    }
#ifdef SERIAL_DEBUG    
    showConfig(config);
#endif    
};

int 	RC_Config::getInput() {
    return config.input;
};

void 	RC_Config::getVersion(char* ver) {
	ver[0] = 'v';
    ver[1] = config.versionH + 48;
	ver[2] = '.';
    ver[3] = config.versionL + 48;
	ver[4] = 0;
};

void 	RC_Config::setInput(int in) {
    if (in == AUDIO_INPUT_MIC || in == AUDIO_INPUT_LINEIN) {
        config.input = in;
        store();
    }
};

float 	RC_Config::getHeadphonesVol() {
    return config.headphonesVol;
};

bool 	RC_Config::setHeadphonesVol(float vol) {
    if (vol >= 0.0 && vol <= 1.00001) {
        config.headphonesVol = vol;
        store();
		return true;
    }
	return false;
};

byte 	RC_Config::getMicGain() {
    return config.micGain;
};

bool	RC_Config::setMicGain(byte gain) {
    if (gain >= 0 && gain <= 63) {
        config.micGain = gain;
        store();
        return true;
    }
    return false;
};

byte 	RC_Config::getLineInLevel() {
    return config.lineInLevel;
};

bool	RC_Config::setLineInLevel(byte lvl) {
    if (lvl >= 0 && lvl <= 15) {
        config.lineInLevel = lvl;
        store();
        return true;
    }
    return false;
};

byte 	RC_Config::getLineOutLevel() {
    return config.lineOutLevel;
};

bool	RC_Config::setLineOutLevel(byte lvl) {
    if (lvl >= 13 && lvl <= 31) {
        config.lineOutLevel = lvl;
        store();
		return true;
    }
	return false;
};

T_Config RC_Config::read() {
    T_Config newConfig;
    EEPROM.get(CONFIG_ADDRESS, newConfig);
    return newConfig;
};

bool RC_Config::isValid(T_Config testConf) {
    return (	strncmp(testConf.header,config.header,2)==0 && \
                testConf.versionH == config.versionH && \
                testConf.versionL == config.versionL );
};

void RC_Config::store() {
    EEPROM.put(CONFIG_ADDRESS, config);
};

