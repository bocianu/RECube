#include "WProgram.h"
#include "RC_Error.h"

RC_Error::RC_Error(int* mode, int error_mode) {
    this->mode = mode;
    this->error_mode = error_mode;
    mute();
}
void RC_Error::set(char* txt) {
    strcpy(msg,txt);
    raise();
}
void RC_Error::set(String txt) {
    txt.toCharArray(msg,txt.length());
    raise();
}
void RC_Error::set(const char* txt) {
    strcpy(msg,txt);
    raise();
}

void RC_Error::add(char* txt) {
    strcat(msg,txt);

}
bool RC_Error::active() {
    return (state == 1);
}
char* RC_Error::get() {
    return msg;
}
char* RC_Error::display() {
    mute();
    return msg;
}

void RC_Error::mute() {
    state = 0;
}

void RC_Error::raise() {
    state = 1;
    *(mode) = error_mode;
}


