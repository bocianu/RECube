#ifndef RC_HardwareSetup_h
#define RC_HardwareSetup_h

// Button pins

#define PIN_RECORD 4
#define PIN_BACK 1
#define PIN_UP 0
#define PIN_DOWN 3
#define PIN_OK 2

// LED pins

#define PIN_LED 5

// Chip Select for SD

#define SD_CS 10

// OLED 

#define OLED_SDA 16
#define OLED_SCL 17

// Restart macro
//#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
//#define CPU_RESTART_VAL 0x5FA0004
//#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);
#define CPU_RESTART SCB_AIRCR = 0x05FA0004;
//#define CPU_RESTART _reboot_Teensyduino_();

#endif


