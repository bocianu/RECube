#ifndef RC_Config_h
#define RC_Config_h

#include <EEPROM.h>
#include "RC_DataStructures.h"

#define VERSION_HI 1
#define VERSION_LO 0

//#define SERIAL_DEBUG

// MODES
#define MODE_STANDBY 0
#define MODE_RECORD 1
#define MODE_PLAY 2
#define MODE_OPTIONS 3
#define MODE_ERROR 99

// bounce time for buttons in ms
#define BOUNCE_TIME 150

// Prefix must have 3 letters
#define FILENAME_PREFIX "REC"
#define DATA_FOLDER "RECUBE"
#define FILES_IN_PLAYER 3

// Config data eeprom address
#define CONFIG_ADDRESS 0

// RC_Config with default values
typedef struct {
    char header[2] = {'R','C'};		// used to recognize fresh eeprom
    byte versionH = VERSION_HI;		// change version to update eeprom with default values
    byte versionL = VERSION_LO;		//

    int input = AUDIO_INPUT_MIC ; 	// AUDIO_INPUT_MIC || AUDIO_INPUT_LINEIN
    float headphonesVol = 0.5;    	// 0.0 - 1.0
    byte micGain = 32;			  	// 0 - 63
    byte lineInLevel = 5;			// 0 - 15
    byte lineOutLevel = 29;			// 13 - 31
} T_Config;

class RC_Config {
    public:
        void 	init();
        int 	getInput();
        void 	setInput(int in);
        float 	getHeadphonesVol();
        bool 	setHeadphonesVol(float vol);
        byte 	getMicGain();
        bool	setMicGain(byte gain);
        byte 	getLineInLevel();
        bool	setLineInLevel(byte lvl);
        byte 	getLineOutLevel();
        bool	setLineOutLevel(byte lvl);
		void 	getVersion (char* ver);

    protected:
        T_Config read();
        bool isValid(T_Config testConf);
        void store();
        T_Config config;
        void showConfig(T_Config conf);
};



#endif


