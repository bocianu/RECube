#include "RC_Recorder.h"

#define SERIAL_DEBUG

// *********************************************************************
// *************************************************** AUDIO SETTINGS
// *********************************************************************

AudioInputI2S            i2sIN;
AudioAnalyzePeak         peak1;
AudioAnalyzePeak         peak2;
AudioRecordQueue         queue1;
AudioRecordQueue         queue2;
AudioConnection          patchCord1(i2sIN, 0, queue1, 0);
AudioConnection          patchCord2(i2sIN, 1, queue2, 0);
AudioConnection          patchCord3(i2sIN, 0, peak1, 0);
AudioConnection          patchCord4(i2sIN, 1, peak2, 0);

AudioPlaySdWav           playWav;
AudioOutputI2S           i2sOUT;
AudioConnection          patchCord5(playWav, 0, i2sOUT, 0);
AudioConnection          patchCord6(playWav, 1, i2sOUT, 1);

AudioControlSGTL5000     sgtl5000_1;

// *********************************************************************
// *************************************************** UTILS
// *********************************************************************

bool RC_Recorder::init() {
    if (!SDReader.cardPresent()) {
        error.set("SD Card error");
        return false;
    }

    AudioMemory(60);
    sgtl5000_1.enable();
    updateSettings();

    if (this->enterDataFolder())
    {
        this->lastNum = this->getLastFileNum();
        return true;
    }
    return false;
}

unsigned long RC_Recorder::LastFileNum() {
    return this->lastNum;
}

AudioAnalyzePeak* RC_Recorder::getPeak(byte num) {
    return (num==1)?&peak1:&peak2;
}

byte RC_Recorder::channelsNum() {
    return (this->myInput == AUDIO_INPUT_MIC)?1:2;
}

bool RC_Recorder::isStereo() {
    return (this->myInput == AUDIO_INPUT_LINEIN);
}

String RC_Recorder::getFilename(unsigned long num) {
    String filename = FILENAME_PREFIX;
    int zeroes = 5;
    String slast = num;
    zeroes -= slast.length();
    while(--zeroes>=0) {
        filename += "0";
    }
    filename += num;
    filename += ".WAV";
    return filename;
}

bool RC_Recorder::enterDataFolder() {
    if (!SD.exists(DATA_FOLDER)) {
        if (!SD.mkdir(DATA_FOLDER)) {
            error.set("Cannot create data folder");
            return false;
        }
    }
    if (!SD.chdir(DATA_FOLDER,true)) {
        error.set("Cannot enter data folder");
        return false;
    }
    return true;
}

unsigned long RC_Recorder::getLastFileNum() {
    if (!SDReader.cardPresent()) {
        error.set("SD Card error");
        return false;
    }
    SD.vwd()->rewind();
    dir_t dir;
    String name;
    int current = 0;
    int last = 0;

    while (SD.vwd()->readDir(&dir) != 0) {
        name = (char*)&dir.name;
        if (name.substring(8,11) == "WAV" && name.substring(0,3) == FILENAME_PREFIX) {
            current = (int)name.substring(3,8).toInt();
            last = (current > last)?current:last;
        }
    }
    this->lastNum = last;
    return last;
}

bool RC_Recorder::gainUp() {
    bool rval;
    if (config.getInput()==AUDIO_INPUT_MIC) {
        rval = config.setMicGain(config.getMicGain()+1);
    } else {
        rval = config.setLineInLevel(config.getLineInLevel()+1);
    };
    if (rval) updateSettings();
    return rval;
}

bool RC_Recorder::gainDown() {
    bool rval;
    if (config.getInput()==AUDIO_INPUT_MIC) {
        rval = config.setMicGain(config.getMicGain()-1);
    } else {
        rval = config.setLineInLevel(config.getLineInLevel()-1);
    };
    if (rval) updateSettings();
    return rval;
}

void RC_Recorder::updateSettings() {
    sgtl5000_1.inputSelect(config.getInput());
    sgtl5000_1.volume(config.getHeadphonesVol());
    sgtl5000_1.micGain(config.getMicGain());
    sgtl5000_1.lineInLevel(config.getLineInLevel());
    sgtl5000_1.lineOutLevel(config.getLineOutLevel());
}

// *********************************************************************
// *************************************************** RECORDER PART
// *********************************************************************

void RC_Recorder::initWavHeader() {
    // Initialize WAV header structure
    strncpy(header.riff , "RIFF", 4);
    header.size = 0;
    strncpy(header.wave , "WAVE", 4);
    strncpy(header.fmt , "fmt ",4);
    header.chunksize = 16;
    header.type = 1;
    header.channels = channelsNum();
    header.samplerate = SAMPLE_RATE;
    header.bytespersec = (channelsNum() * SAMPLE_RATE * BITS_PER_SAMPLE) / 8;
    header.blockalign = (channelsNum() * BITS_PER_SAMPLE) / 8;
    header.bps = BITS_PER_SAMPLE;
    strncpy(header.data , "data", 4);
    header.datasize = 0;
}

bool RC_Recorder::writeWavHeader() {
    if(!frec.isOpen()) {
        error.set("No access to file");
        return false;
    }

    initWavHeader();
    header.channels = channelsNum();
    header.bytespersec = (channelsNum() * SAMPLE_RATE * BITS_PER_SAMPLE) / 8;
    header.blockalign = (channelsNum() * BITS_PER_SAMPLE) / 8;
    header.size = 0;
    header.datasize = 0;

    if (!frec.write((char*)&header, sizeof(header))) {
        error.set("Error Writing Wav header");
        return false;
    }
    return true;
}

bool RC_Recorder::updateWavHeader() {
    unsigned long fSize;
    if(!frec.isOpen()) {
        error.set("No access to file");
        return false;
    }

    fSize = frec.fileSize();
    header.size = fSize - 8;
    header.datasize = fSize - 44;

    frec.seek(0);
    if (!frec.write((char*)&header, sizeof(header))) {
        error.set("Error Updating Wav header");
        return false;
    }
    return true;
}

bool RC_Recorder::startRecording() {
    if (!SDReader.cardPresent()) {
        error.set("SD Card error");
        return false;
    }
    char filename[13];
    unsigned long num = getLastFileNum();
    if (num == 99999) {
        error.set("Maximum number of 99999 files reached! You are mad.");
        return false;
    }
    getFilename(num + 1).toCharArray(filename,13);
    if (SD.exists(filename)) {
        error.set("File already exists: ");
        error.add(filename);
        return false;
    }
    frec = SD.open(filename, FILE_WRITE);
    if (frec) {
        if (writeWavHeader()) {
            queue1.begin();
            if (isStereo()) {
                queue2.begin();
            }
            byterec = 0;
            timerec = 0;
            writeCount = 0;
            writeTime = 0;
            lastmode = mode;
            mode = MODE_RECORD;
            digitalWrite(PIN_LED, HIGH);
            return true;
        }
    }
    error.set("Error accessing file: ");
    error.add(filename);
    return false;
}

bool RC_Recorder::keepRecording() {
    unsigned long prev;
    uint16_t buffer[256];
    bool toStore = false;
    uint16_t left[128];
    uint16_t right[128];
    uint16_t offset;
    byte i;
    elapsedMicros usec = 0;

    if (isStereo()) {  // Stereo buffer fill
        if (queue1.available() >= 1 && queue2.available() >= 1) {

            memcpy(left, queue1.readBuffer(), 256);
            queue1.freeBuffer();
            memcpy(right, queue2.readBuffer(), 256);
            queue2.freeBuffer();

            for (i=0; i<128; i++) {
                offset = i << 1;
                buffer[offset] = left[i];
                buffer[offset+1] = right[i];
            }

            toStore = true;
        }
    } else { // Mono buffer fill
        if (queue1.available() >= 2) {

            memcpy(buffer, queue1.readBuffer(), 256);
            queue1.freeBuffer();
            memcpy(buffer+128, queue1.readBuffer(), 256);
            queue1.freeBuffer();

            toStore = true;
        }
    }
    if (toStore) {
        if (frec.write(buffer, 512)) {
            byterec += 512;
            prev = timerec;
            timerec = byterec / header.bytespersec;
            writeCount++;
            writeTime += usec;

#ifdef SERIAL_DEBUG
            if ( timerec != prev) {
                Serial.print("seconds : ");
                Serial.println(timerec);
                Serial.print("avg write time : ");
                Serial.println(writeTime/writeCount);

            }
#endif
            return true;
        }
        else {
            error.set("Problem writing file.");
            updateWavHeader();
            return false;
        }
    }
    return false;
}

bool RC_Recorder::stopRecording() {
    queue1.end();
    queue2.end();
    if (mode == MODE_RECORD) {
        while (keepRecording()) {
        };
        if (updateWavHeader()) {
            mode = lastmode;
        }
    }
    frec.close();
    currentFileNum = getLastFileNum();
    getPlayerFiles();
    digitalWrite(PIN_LED, LOW);
    return true;
}

// *********************************************************************
// *************************************************** PLAYER PART
// *********************************************************************

bool RC_Recorder::startPlayer() {
    long unsigned fnum;
    fnum = getPlayerFiles();
    if (fnum>0) {
        mode = MODE_PLAY;
        return true;
    }
    else {
        return false;
    }
}

unsigned long RC_Recorder::getPlayerFiles() {
    memset(fileList,0,sizeof(fileList));
    if (currentFileNum == 0) {
        currentFileNum = getLastFileNum();
    }
    if (currentFileNum == 0) {
        return 0;
    }
    SD.vwd()->rewind();
    dir_t dir;
    String name;
    unsigned long fnum = 0;
    unsigned long listCount = 0;
    byte playerListIndex = 0;
    bool found = false;
    nextFileNum = 0;
    prevFileNum = 0;
    filename tempFileList[FILES_IN_PLAYER];
    byte listShift;

    while (SD.vwd()->readDir(&dir) != 0) {
        name = (char*)&dir.name;
        if (name.substring(8,11) == "WAV" && name.substring(0,3) == FILENAME_PREFIX) {
            name.toCharArray(fileList[playerListIndex],9);
            //strcpy(fileList[playerListIndex]+8,fileList[playerListIndex]+9);
            fnum = (int)name.substring(3,8).toInt();
            listCount++;
            if (found) {
                if (nextFileNum==0) {
                    nextFileNum = fnum;
                }
                if (listCount >= FILES_IN_PLAYER) {
                    playerListIndex++;
                    break;
                }
            } else {
                if (fnum == currentFileNum) {
                    found = true;
                    listPointer = playerListIndex;
                    getFilename(fnum).toCharArray(currentFileName,13);
                } else {
                    prevFileNum = fnum;
                }
            }
            playerListIndex++;
            playerListIndex %= FILES_IN_PLAYER;
        }
    }

    if (listCount>FILES_IN_PLAYER) {
        // aligning file list due to shift
        listShift = FILES_IN_PLAYER - playerListIndex;
        memcpy(tempFileList,fileList,sizeof(fileList));
        for (byte i=0; i<FILES_IN_PLAYER; i++) {
            strcpy(fileList[i],tempFileList[(FILES_IN_PLAYER - listShift + i)%FILES_IN_PLAYER]);
        }
        // recalculating list pointer
        listPointer = (listPointer + listShift) % FILES_IN_PLAYER;
    }

    return fnum;
}

bool RC_Recorder::selectPrev() {
    if (prevFileNum!=0) {
        currentFileNum = prevFileNum;
        getPlayerFiles();
        return true;
    }
    return false;
}

bool RC_Recorder::selectNext() {
    if (nextFileNum!=0) {
        currentFileNum = nextFileNum;
        getPlayerFiles();
        return true;
    }
    return false;
}

bool RC_Recorder::startPlayingCurrent() {
    return playWav.play(currentFileName);
}

void RC_Recorder::stopPlaying() {
    playWav.stop();
}

bool RC_Recorder::isPlaying() {
    return playWav.isPlaying();
};

unsigned long RC_Recorder::playerPos() {
    return playWav.positionMillis()/1000;
};

unsigned long RC_Recorder::playerLen() {
    return playWav.lengthMillis()/1000;
};

void RC_Recorder::removeCurrent() {
    if (currentFileNum!=0) {
        SD.remove(currentFileName);
        if (nextFileNum!=0) {
            currentFileNum = nextFileNum;
        } else {
            currentFileNum = prevFileNum;
        }
        getPlayerFiles();
    }
}

bool RC_Recorder::keepPlaying() {
    if (prevPlayerState != isPlaying()) {
        prevPlayerState = isPlaying();
        return false;
    }
    return true;
}

